﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags
{
    public static class HelpFuncs
    {
        public static class DynamicFunctions
        {
            public static TemplateFunction Length = new TemplateFunction()
            {
                FunctionPatern = @"\$length\s*\(\s*(([a-zA-Z._0-9]+)|(\s*))\s*\)",
                Function = HelpFuncs.Length
            };

            public static TemplateFunction State = new TemplateFunction()
            {
                FunctionPatern = @"\$state\s*\(\s*(([a-zA-Z._0-9]+)|(\s*))\s*\)",
                Function = HelpFuncs.State
            };

            public static TemplateFunction AsArray = new TemplateFunction()
            {
                FunctionPatern = @"\$asArray\s*\(\s*(([a-zA-Z._0-9]+)|(\s*))\s*\)",
                Function = HelpFuncs.AsArray
            };
            public static TemplateFunction Remove = new TemplateFunction()
            {
                FunctionPatern = @"\$remove\s*\(\s*(([a-zA-Z._0-9,/\\*+\-%]+)|(\s*))\s*\)",
                Function = HelpFuncs.Remove
            };
        }

        private static DataTable calculator = new DataTable();
        public const string ValuePattern = @"\s*([a-zA-Z._0-9]+)\s*";
        public const string ValuePatternEmpty = @"^([a-zA-Z._0-9]+)$";
        public static T[] Push<T>(this T[] arr, T obj)
        {
            Array.Resize(ref arr, arr.Length + 1);
            arr[arr.Length - 1] = obj;
            return arr;
        }

        public static JToken AsArray(string command, JToken jb, TemplateFunction[] arr)
        {
            JToken result = null;
            if (!string.IsNullOrEmpty(command))
            {
                jb = new JValue(ExecuteGetValue(command, jb, arr));
            }
            if (jb.Type == JTokenType.Array)
                return jb;

            if (jb.Type == JTokenType.String)
            {
                var val = jb.ToString().Trim();
                if (val.Length > 3)
                {
                    var xchar = val[0];
                    if (xchar == '\'' || xchar == '"')
                    {
                        val = val.Trim(xchar);
                        result = JArray.Parse(val);
                    }
                    else
                    {
                        if (xchar == '[')
                        {
                            result = JArray.Parse(val);
                        }
                        else
                        {

                            throw new ArgumentException(string.Format("argument not a array source:'{0}'",
                                jb));
                        }
                    }
                }
                else
                {
                    throw new ArgumentException(string.Format("argument not a array source:'{0}'", jb));
                }
            }
            return result;
        }

        public static string StringCompile(this string command)
        {
            string result = string.Empty;
            try
            {
                //Math & if
                result = calculator.Compute(command, string.Empty).ToString();
            }
            catch
            {
                result = command;
            }
            return result;
        }

        public static JToken Remove(string command, JToken jb, TemplateFunction[] arr)
        {
            var spl = command.Split(',');
            var result = ExecuteGetValue(string.Join(",", spl.Take(spl.Length - 1)), jb, arr);
            var indexStr = ExecuteGetValue(spl[spl.Length - 1], jb, arr);
            var index = int.Parse(indexStr.Trim());
            return index > result.Length ? result : result.Remove(index);
        }

        public static bool TryGetJTokenValue(this JToken jb, string key, out JToken jtoken)
        {
            try
            {
                jtoken = jb[key];
                return true;
            }
            catch (ArgumentException)
            {
                // : ) todo Bug
            }
            catch (InvalidOperationException)
            {
                // :) bug in newtonsoft
            }
            jtoken = null;
            return false;
        }
        public static JToken GetValue(this JToken jb, string command, TemplateFunction[] arr = null, bool checkPerent = true)
        {
            //optimize
            if (command.IndexOf("$", System.StringComparison.Ordinal) > -1)
            {
                var match = Match.Empty;
                for (var k = arr.Length - 1; k >= 0; k--)
                {
                    var fn = arr[k];
                    match = Regex.Match(command, fn.FunctionPatern);
                    int eps = 0;
                    if (match != Match.Empty)
                    {
                        int state = match.Value.IndexOf('(');
                        var temp = match.Value.Substring(state + 1, match.Value.LastIndexOf(')') - state - 1);
                        int saveCommandLength = command.Length;
                        command = command.Remove(match.Index + eps, match.Value.Length);
                        return fn.Function(temp, jb, arr);
                        //bug double name funtions
                    }
                }
            }
            int indexofdot = command.IndexOf('.');
            string key = indexofdot >= 0 ? command.Substring(0, indexofdot) : command;
            JToken jbSend = null;
            try
            {
                jb.TryGetJTokenValue(key, out jbSend);
            }
            catch (NullReferenceException)
            {
                return JToken.FromObject(command);
            }
            while (jbSend == null && checkPerent)
            {
                try
                {
                    jb = jb.Parent;
                    jb.TryGetJTokenValue(key, out jbSend);
                }
                catch (NullReferenceException)
                {
                    return JToken.FromObject(command);
                }
            }
            if (indexofdot > 0)
            {
                return jbSend.GetValue(command.Remove(0, indexofdot + 1), arr, false);
            }
            return jbSend;

        }


        public static JToken State(string command, JToken jb, TemplateFunction[] arr)
        {
            command = command.Trim();
            if (string.IsNullOrEmpty(command))
                return jb;
            if (Regex.IsMatch(command, ValuePatternEmpty))
                return jb.GetValue(command, arr);
            return command.ExecuteGetValue(jb, arr);
        }

        public static JToken Length(string command, JToken jb, TemplateFunction[] arr)
        {
            JToken jtoken = null;
            if (!jb.TryGetJTokenValue(command, out jtoken))
            {
                var result = ExecuteGetValue(command, jb, arr);
                return new JValue(result.Length);
            }

            try
            {
                var helpsx = HelpFuncs.AsArray(command, jb, arr);
                if (helpsx != null && helpsx.Type == JTokenType.Array)
                    return helpsx.Count();
            }
            catch (Exception ex) { }
            if (jtoken == null || jtoken.Type == JTokenType.Null || jtoken.Type == JTokenType.None || jtoken.Type == JTokenType.Undefined)
                return JToken.Parse("0");
            return jtoken.Type == JTokenType.Array ? jtoken.Count() : jtoken.ToString().Length;
        }


        //TODO PERFORM
        public static string ExecuteGetValue(this string command, JToken jb,
            params TemplateFunction[] arr)
        {
            command = command.Trim();
            var list = new List<KeyValue>();
            var match = Match.Empty;
            int eps = 0;
            for (var k = arr.Length - 1; k >= 0; k--)
            {
                var fn = arr[k];
                match = Regex.Match(command, fn.FunctionPatern);
                eps = 0;
                while (match != Match.Empty)
                {
                    int state = match.Value.IndexOf('(');
                    var temp = match.Value.Substring(state + 1, match.Value.LastIndexOf(')') - state - 1);
                    int saveCommandLength = command.Length;
                    command = command.Remove(match.Index + eps, match.Value.Length);
                    command = command.Insert(match.Index + eps, fn.Function(temp, jb, arr).ToString());
                    eps = eps + (command.Length - saveCommandLength);
                    match = match.NextMatch();
                    //bug double name funtions
                }
            }
            match = Regex.Match(command, ValuePattern);
            //refil values
            while (match != Match.Empty)
            {
                var tval = match.Value.Trim();
                if (tval.Equals("AND") || tval.Equals("OR") || tval.Equals("NOT"))
                {
                    match = match.NextMatch();
                    continue;
                }
                list.Add(new KeyValue() { Key = match, Value = GetValue(jb, match.Value) });

                match = match.NextMatch();
            }
            var result = new StringBuilder(command);
            //execute compar
            eps = 0;
            for (int i = list.Count - 1; i >= 0; i--)
            {
                var t = list[i];
                var resultOldLength = result.Length;
                if (t.Value == null)
                    throw new EntryPointNotFoundException(t.Key.ToString());
                switch (t.Value.Type)
                {
                    case JTokenType.Boolean:
                    case JTokenType.Bytes:
                    case JTokenType.Float:
                    case JTokenType.Integer:
                        list.RemoveAt(i);
                        //result.Replace(t.Key.Value, t.Value.ToString());
                        result.Remove(t.Key.Index, t.Key.ToString().Length); //+ eps
                        result.Insert(t.Key.Index, t.Value);

                        break;
                    case JTokenType.String:

                        float tpval = 0;
                        list.RemoveAt(i);
                        var temp = t.Value.ToString();
                        if (float.TryParse(temp, out tpval))
                        {
                            if (temp.StartsWith("0"))
                            {
                                if (temp.Contains(".") && Math.Abs(tpval) < 1 || tpval == 0)
                                {
                                    result.Remove(t.Key.Index, t.Key.ToString().Length); // + eps
                                    result.Insert(t.Key.Index, t.Value);
                                    break;
                                }
                            }
                            else
                            {
                                result.Remove(t.Key.Index, t.Key.ToString().Length); // + eps
                                result.Insert(t.Key.Index, t.Value);
                                break;
                            }
                        }
                        var text_forReplace = string.Format("'{0}'", t.Value.ToString().Replace("'", "\'"));
                        result.Remove(t.Key.Index, t.Key.ToString().Length); // + eps
                        result.Insert(t.Key.Index, text_forReplace);
                        break;
                    default:
                        // var tmp = string.Format("{0}", t.Key.Value);
                        //  result.Replace(t.Key.Value, tmp);
                        break;
                }
                //eps = eps - (result.Length - resultOldLength);
            }
            result = new StringBuilder(result.ToString().StringCompile().Trim('\''));
            int removeAtIndex = 0;
            string tempValue = string.Empty;
            int indexSaver = 0;
            foreach (var item in list)
            {

                removeAtIndex = result.IndexOf(item.Key.Value, indexSaver);
                if (removeAtIndex == -1)
                {
                    continue;
                }
                result.Remove(removeAtIndex, item.Key.Value.Length);
                tempValue = item.Value.ToString();
                result.Insert(removeAtIndex, tempValue);
                indexSaver = removeAtIndex + tempValue.Length;
            }
            return result.ToString();
        }


    }
}
