﻿namespace ReEraser.Tags
{
    public enum ActionResult
    {
         Unknown,
         Error,
         Ok,
         Exception,
         Continue,
         Break
    }
}