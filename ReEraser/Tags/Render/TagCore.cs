﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags.Render
{
    public abstract class TagCore
    {
        public TagCore Parent { set; get; }

        protected StringBuilder Template { set; get; }

        protected string Source { set; get; }

        public abstract void AddChildNode(TagCore tag);

        public override string ToString()
        {
            return string.Format("Tag_type: {0}  Tag_source: {1}   Tag_template:{2}", GetTagName(), Source, Template);
        }


        public virtual bool CanHaveChild()
        {
            return false;
        }


        public abstract string GetTagName();
        public abstract bool IsClose();
        public abstract void Close(string tag);

        public virtual void InitializeSource(string source, StringBuilder template)
        {
            Source = source;
            Template = template;
        }
        public abstract ActionResult Build(JToken jobject, StringBuilder result, params TemplateFunction[] arr);
        public StringBuilder BuildObject<T>(T obj) => Build(JsonConvert.SerializeObject(obj));
        public StringBuilder Build(string json)
        {
            var bld = new StringBuilder();
            json = json.Trim();
            if (json.Length < 2)
            {
                throw new ArgumentException(json);
            }
            if (json[0] == '[')
            {
                Build(JArray.Parse(json), bld);
            }
            else
            {
                Build(JObject.Parse(json), bld);
            }
            return bld;
        }
    }
}
