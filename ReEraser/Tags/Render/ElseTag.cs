﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags.Render
{
    public class ElseTag : TagCore
    {
        public ElseTag()
        {
            ChildNodes = new List<TagCore>();
        }

        protected List<TagCore> ChildNodes;

        private bool isClose;
        public override void AddChildNode(TagCore tag)
        {
            if (isClose)
                throw new AccessViolationException("else tag is closed:" + Template);
            tag.Parent = this;
            ChildNodes.Add(tag);
        }

        public override string GetTagName()
        {
            return "else";
        }

        public override bool IsClose()
        {
            return isClose;
        }

        public override bool CanHaveChild()
        {
            return true;
        }

        public override void Close(string tag)
        {
            if (Parent.GetTagName() == tag)
            {
                isClose = true;
                Parent.Close(tag);
            }
        }
        public override void InitializeSource(string source, StringBuilder template)
        {
            this.Source = source;
            var tmp = new Tag();
            tmp.InitializeSource(string.Empty, template);
            ChildNodes.Add(tmp);
        }

        public override ActionResult Build(JToken jobject, StringBuilder result, params TemplateFunction[] arr)
        {

            if (!isClose)
                throw new AccessViolationException("if tag not closed:" + Template);

            foreach (var childNode in ChildNodes)
            {
                var actionResult = childNode.Build(jobject, result, arr);
                if (actionResult != ActionResult.Ok)
                    return actionResult;
            }
            return ActionResult.Ok;
        }
    }
}
