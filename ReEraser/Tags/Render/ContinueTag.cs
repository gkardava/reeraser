﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags.Render
{
    public class ContinueTag:TagCore
    {
        public override void AddChildNode(TagCore tag)
        {
            throw new NotImplementedException();
        }

        public override string GetTagName()
        {
            return "continue";
        }

        public override bool IsClose()
        {
            return true;
        }

        public override void Close(string tag)
        {
        }

        public override ActionResult Build(JToken jobject, StringBuilder result, params TemplateFunction[] arr)
        {
            return ActionResult.Continue;
        }
    }
}
