﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags.Render
{
    public class Tag : TagCore
    {
        //public static readonly string TagPattern = @"\{\{(\s*)([a-zA-Z.0-9]+(\s*))\}\}";
        public static readonly string TagClearPattern = @"\{|\s|\}";
       
        public static string ClearKeys(string key)
        {
            return Regex.Replace(key, TagClearPattern, string.Empty);
        }
        public override string GetTagName()
        {
            return string.Empty;
        }

        public override bool IsClose()
        {
            return true;
        }

        public override void Close(string tag)
        {
            throw  new NotImplementedException();
        }


        private IEnumerable<Pack> GetTagCommands(StringBuilder text)
        {
            int endIndex = 0;
            int textSizeOld = 0;
            while (endIndex != -1)
            {
                var packeg = new Pack { FoundIndex = text.IndexOf("{{", endIndex) };
                if (packeg.FoundIndex == -1)
                    yield break;
                endIndex = text.IndexOf("}}");
                endIndex += 2;
                packeg.Key = text.CopyTo(packeg.FoundIndex, endIndex).ToString();
                textSizeOld = text.Length;
                yield return packeg;
                endIndex = endIndex-(textSizeOld - text.Length);
            }
        }
        public override ActionResult Build(JToken jobject, StringBuilder result, params TemplateFunction[] arr)
        {

            var tmp = Template.CopyTo(0, Template.Length);
            foreach (var item in GetTagCommands(tmp))
                {
                    tmp.Remove(item.FoundIndex, item.Key.Length);

                    tmp.Insert(item.FoundIndex, ClearKeys(item.Key).ExecuteGetValue(jobject, arr));
                }
            result.Append(tmp);
            return ActionResult.Ok;
        }

        public override void AddChildNode(TagCore tag)
        {
            throw new NotImplementedException();
        }
    }
}
