﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags.Render
{
    sealed public class Parent : TagCore
    {
        private List<TagCore> listNode = new List<TagCore>();

        public Parent()
        {
            listNode = new List<TagCore>();
        }
        public override void AddChildNode(TagCore tag)
        {
            tag.Parent = this;
            listNode.Add(tag);
        }

        public override string GetTagName()
        {
            return "parent";
        }

        public override bool IsClose()
        {
            return false;
        }

        public override void Close(string tag)
        {

            throw new NotImplementedException(string.Format("Cannot close root tag! close command:{0}", tag));
        }

        public override bool CanHaveChild()
        {
            return true;
        }

        public override ActionResult Build(JToken jobject, StringBuilder result, params TemplateFunction[] arr)
        {
            if (arr == null)
            {
                arr = new TemplateFunction[0];
            }

            arr = arr.Push(HelpFuncs.DynamicFunctions.Length);
            arr = arr.Push(HelpFuncs.DynamicFunctions.State);
            arr = arr.Push(HelpFuncs.DynamicFunctions.AsArray);
            arr = arr.Push(HelpFuncs.DynamicFunctions.Remove);

            foreach (var item in listNode)
            {
                //todo Fix
                if (ActionResult.Ok != item.Build(jobject, result, arr))
                    return ActionResult.Unknown;
            }
            return ActionResult.Ok;
        }
    }
}
