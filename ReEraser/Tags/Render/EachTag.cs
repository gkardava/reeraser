﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.XPath;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags.Render
{
    public class EachTag : TagCore
    {
        public EachTag()
        {
            listNode = new List<TagCore>();
            declaredValueName = string.Empty;
        }

        protected string declaredValueName;
        protected List<TagCore> listNode;
        public override string GetTagName()
        {
            return "each";
        }

        private bool isClose;

        public override bool IsClose()
        {
            return isClose;
        }

        public override void AddChildNode(TagCore tag)
        {
            if (isClose)
                throw new AccessViolationException("each tag is closed:" + Template);
            tag.Parent = this;
            listNode.Add(tag);
        }
        public override void Close(string tag)
        {
            if (GetTagName() == tag)
                isClose = true;
        }

        public override bool CanHaveChild()
        {
            return true;
        }

        public override void InitializeSource(string source, StringBuilder template)
        {
            this.Source = source;
            var tmp = new Tag();
            tmp.InitializeSource(string.Empty, template);
            listNode.Add(tmp);
        }

        public override ActionResult Build(JToken jobject, StringBuilder result, params TemplateFunction[] arr)
        {
            if (!isClose)
                throw new AccessViolationException("each tag not closed:" + Template);
            //Declare Value
            if (declaredValueName.Length == 0)
                if (Source.IndexOf(':') > -1)
                {
                    var comRes = Source.Split(':');
                    if (comRes.Length != 2)
                        throw new ArgumentException(string.Format("each argument not correct source:'{0}'", Source));
                    declaredValueName = comRes[0].Trim();
                    if (declaredValueName.Length < 1 || !Regex.IsMatch(declaredValueName, HelpFuncs.ValuePattern))
                        throw new ArgumentException(string.Format("each argument not value source:'{0}'", declaredValueName));
                    Source = comRes[1];
                }
                else
                {
                    declaredValueName = Regex.IsMatch(Source, HelpFuncs.ValuePattern) ? Source : string.Empty;
                }
            var tmp_eachArray = jobject.GetValue(Source, arr);

            if (tmp_eachArray.Type == JTokenType.Null || tmp_eachArray.Type == JTokenType.Undefined ||
                tmp_eachArray.Type == JTokenType.None)
            {
                return ActionResult.Ok;
            }


            if (tmp_eachArray.Type != JTokenType.Array)
            {
                if (tmp_eachArray.Type == JTokenType.String)
                {
                    tmp_eachArray = HelpFuncs.AsArray(Source, tmp_eachArray, arr);
                    if (tmp_eachArray == null || tmp_eachArray.Type != JTokenType.Array)
                        throw new ArgumentException(string.Format("each argument not a array source:'{0}'", Source));
                }
                else
                {
                    throw new ArgumentException(string.Format("each argument not a array source:'{0}'", Source));
                }

            }
            TemplateFunction itemFunc = default(TemplateFunction);
            TemplateFunction indexOf = default(TemplateFunction);
            TemplateFunction length = default(TemplateFunction);
            if (declaredValueName.Length != 0)
            {
                if (arr == null)
                    arr = new TemplateFunction[0];
                Array.Resize(ref arr, arr.Length + 3);
                itemFunc = new TemplateFunction { FunctionPatern = @"\$state\(\s*" + declaredValueName + @"\s*\)" };
                indexOf = new TemplateFunction { FunctionPatern = @"\$index\(\s*" + declaredValueName + @"\s*\)" };
                length = new TemplateFunction { FunctionPatern = @"\$length\(\s*" + declaredValueName + @"\s*\)" };
            }
            int i = 0;
            var actionResult = ActionResult.Continue;

            length.Function = (c1, c2, c3) => tmp_eachArray.Count();
            foreach (JToken item in tmp_eachArray)
            {
                if (declaredValueName.Length != 0)
                {
                    var item1 = item;
                    itemFunc.Function = (a1, a2, a3) => item1;
                    int i1 = i;
                    indexOf.Function = (b1, b2, b3) => i1;
                    arr[arr.Length - 1] = itemFunc;
                    arr[arr.Length - 2] = indexOf;
                    arr[arr.Length - 3] = length;
                }
                foreach (var tagCore in listNode)
                {
                    actionResult = tagCore.Build(item, result, arr);
                    if (actionResult != ActionResult.Ok)
                        break;
                }
                if (actionResult == ActionResult.Continue)
                    continue;
                if (actionResult == ActionResult.Break)
                    break;
                i++;
            }
            return ActionResult.Ok;
        }
    }
}
