﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags.Render
{
    public class IfTag : TagCore
    {

        public IfTag()
        {
            ChildNodes = new List<TagCore>();
        }

        protected List<TagCore> ChildNodes;
        protected TagCore ElseTag { set; get; }
        private bool isClose;
        public override void AddChildNode(TagCore tag)
        {
            if (isClose)
                throw new AccessViolationException("if tag is closed");
            tag.Parent = this;
            if (tag.GetTagName() == "else")
            {
                isClose = true;
                ElseTag = tag;
                return;
            }
            ChildNodes.Add(tag);
        }
        public override void InitializeSource(string source, StringBuilder template)
        {
            this.Source = source;
            var tmp = new Tag();
            tmp.InitializeSource(string.Empty, template);
            ChildNodes.Add(tmp);
        }

        public override string GetTagName()
        {
            return "if";
        }

        public override bool IsClose()
        {
            return isClose;
        }

        public override bool CanHaveChild()
        {
            return true;
        }

        public override void Close(string tag)
        {
            if (GetTagName() == tag)
                isClose = true;
        }


        public override ActionResult Build(JToken jobject, StringBuilder result, params TemplateFunction[] arr)
        {
            if (!isClose)
                throw new AccessViolationException("if tag not closed" + Template);

            bool rb;
            bool.TryParse(Source.ExecuteGetValue(jobject,arr), out rb);
            if (rb)
            {
                foreach (var childNode in ChildNodes)
                {
                    var actionResult = childNode.Build(jobject, result, arr);
                    if (actionResult != ActionResult.Ok)
                        return actionResult;
                }
                return ActionResult.Ok;
            }
            return ElseTag==null?ActionResult.Ok : ElseTag.Build(jobject, result, arr);
        }
    }
}
