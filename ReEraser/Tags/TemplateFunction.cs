﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace ReEraser.Tags
{
    public struct TemplateFunction
    {
        public Func<string, JToken, TemplateFunction[],JToken>Function;

        public string FunctionPatern { set; get; }

        public override bool Equals(object obj)
        {
            if (obj is TemplateFunction)
            {
                return ((TemplateFunction)obj).FunctionPatern.Equals(FunctionPatern);
            }
            return false;
        }
    }
}
