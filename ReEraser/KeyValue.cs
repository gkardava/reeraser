﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace ReEraser
{
    public struct KeyValue
    {
        public JToken Value { set; get; }
        public Match Key { set; get; }
        public override bool Equals(object obj)
        {
            if (obj is KeyValue)
            {
                var res = ((KeyValue) obj);
                return res.Key.Equals(Key) && res.Value.Equals(Value);
            }
            return false;
        }
        
    }
}
