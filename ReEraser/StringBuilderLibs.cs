﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReEraser
{
    public static class StringBuilderLibs
    {
        public static StringBuilder CopyTo(this StringBuilder haystack, int startIndex, int toIndex)
        {
            if(toIndex== startIndex)
                return  new StringBuilder();
            if (startIndex >= 0 && toIndex >= 0 && startIndex < haystack.Length && toIndex <= haystack.Length && toIndex > startIndex)
            {
                var res = new StringBuilder();
                do
                {
                    res.Append(haystack[startIndex]);
                    //todo skeep
                } while (++startIndex < toIndex);
                return res;
            }
            throw new ArgumentOutOfRangeException(String.Format("copy error on at {0}",startIndex));
        }

        public static bool Contains(this StringBuilder haystack, string needle)
        {
            return haystack.IndexOf(needle) != -1;
        }

        public static int IndexOf(this StringBuilder haystack, string needle)
        {
           return IndexOf(haystack, needle, 0);
        }

        public static int IndexOf(this StringBuilder theStringBuilder, string value, int startPosition)
        {
            const int NOT_FOUND = -1;
            if (theStringBuilder == null)
            {
                return NOT_FOUND;
            }
            if (String.IsNullOrEmpty(value))
            {
                return NOT_FOUND;
            }
            int count = theStringBuilder.Length;
            int len = value.Length;
            if (count < len)
            {
                return NOT_FOUND;
            }
            int loopEnd = count - len + 1;
            if (startPosition >= loopEnd)
            {
                return NOT_FOUND;
            }
            for (int loop = startPosition; loop < loopEnd; loop++)
            {
                bool found = true;
                for (int innerLoop = 0; innerLoop < len; innerLoop++)
                {
                    if (theStringBuilder[loop + innerLoop] != value[innerLoop])
                    {
                        found = false;
                        break;
                    }
                }
                if (found)
                {
                    return loop;
                }
            }
            return NOT_FOUND;
        }
    }
}
