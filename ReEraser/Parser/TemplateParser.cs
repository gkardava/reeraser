﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using ReEraser.Tags.Render;

namespace ReEraser.Parser
{
    public class TemplateParser
    {
        public static readonly string CommandPattern = @"(\s|\{)\#([a-z]+\b)(\s|\})";
        public static readonly string CommandKeyTrimPattern = @"\#|\{|\s|\}";
        public static readonly string FnPattern = @"{{(\s*)(#\w*)(\s*)([a-zA-Z.0-9\:\%\>\/\<\=\$\+\-\*\)\(\ ]+)(\s*)}}";
        public static IEnumerable<NodePack> GetNodePacks(string text)
        {
            return GetNodePacks(new StringBuilder(text));
        }

        public static string GetSourceAndKey(string command, out string source)
        {
            command = command.Trim('{', '}', ' ', '#');
            var index = command.IndexOf(' ');
            if (index > -1)
            {
                source = command.Substring(index).Trim();
                return command.Substring(0, index).Trim();
            }
            source = string.Empty;
            return command;
        }

        public static IEnumerable<NodePack> GetNodePacks(StringBuilder text)
        {
            var match = Regex.Match(text.ToString(), FnPattern);
            int startIndex = 0;
            string tmp;
            if (match != Match.Empty)
            {//Parent simple node
                yield return new NodePack() { Template = text.CopyTo(0, match.Index) };
            }
            while (match != Match.Empty)
            {
                tmp = string.Empty;
                var nextMatch = match.NextMatch();
                var node = new NodePack()
                {
                    Key = GetSourceAndKey(match.Value, out tmp),
                    Source = tmp
                };
                if (node.Key != "end")
                {
                    node.Template = text.CopyTo(match.Index + match.Length, nextMatch.Index);
                }
                else
                {
                    yield return node;
                    node = new NodePack();
                    node.Template = text.CopyTo(match.Index + match.Length, nextMatch.Index == 0 ? text.Length : nextMatch.Index);
                }
                yield return node;
                startIndex = match.Length + match.Index;
                if (match == Match.Empty)
                    break;
                match = nextMatch;
            }
            if (startIndex == 0)
                yield return (new NodePack()
                {
                    Template = text.CopyTo(startIndex, text.Length)
                });
        }

        public static TagCore CreateTagBuilder(string text)
        {
            return CreateTagBuilder(new StringBuilder(text));
        }

        public static TagCore CreateTagBuilder(StringBuilder text)
        {
            TagCore parent = new Parent();
            TagCore child = null;
            TagCore tmp = null;
            foreach (var node in GetNodePacks(text))
            {
                //select non close pherent
                while (parent.IsClose())
                {
                    if (parent.Parent != parent)
                        parent = parent.Parent;
                    else
                        throw new ArgumentException(string.Format("unable to close `{0}` ,with state: `{1}`,last command:`{2}`", parent.GetTagName(), node, tmp));

                }
                switch (node.Key)
                {
                    case null:
                    case "":
                    case "tag":
                        child = new Tag();
                        break;
                    case "each":
                        child = new EachTag();
                        break;
                    case "if":
                        child = new IfTag();
                        break;
                    case "else":
                        if (parent.GetTagName() == "if")
                            child = new ElseTag();
                        else
                            throw new ArgumentException(string.Format("`else` tag without `if` details. state {0}, command:{1}", parent.GetTagName(), node));
                        break;
                    case "continue":
                        tmp = parent;
                        do
                        {
                            if (tmp.GetTagName() == "each")
                            {
                                child = new ContinueTag();
                                break;
                            }
                            tmp = tmp.Parent;
                        } while (tmp != null);
                        break;
                    case "break":
                        tmp = parent;
                        do
                        {
                            if (tmp.GetTagName() == "each")
                            {
                                child = new BreakTag();
                                break;
                            }
                            tmp = tmp.Parent;
                        } while (tmp != null);
                        break;
                    case "end":
                        parent.Close(node.Source);
                        continue;
                    default:
                        throw new ArgumentException("unable to identify key:" + node);
                }
                if (child == null)
                    throw new ArgumentException("could not found tag:" + node);
                parent.AddChildNode(child);
                if (child.CanHaveChild())
                    parent = child;
                child.InitializeSource(node.Source, node.Template);
            }
            //select non close pherent
            while (parent.IsClose())
            {
                parent = parent.Parent;
            }
            if (parent.GetTagName() != "parent")
            {
                throw new ArgumentException(string.Format("somesing is wrong  {0}", parent));
            }
            return parent;
        }
    }
}