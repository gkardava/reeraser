﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ReEraser.Parser
{
    public class NodePack
    {
        public string Key { set; get; }
        public StringBuilder Template { set; get; }
        public string Source { set; get; }

        public override string ToString()
        {
            return string.Format("Key:{0},Source:{1}", Key, Source);
        }
    }
}
