﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using ReEraser.Parser;
using ReEraser.Tags;
using ReEraser.Tags.Render;
using ReEraser;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Threading;
using Mustache;

namespace TestProject
{
    [TestFixture]
    public class TemplateTester
    {

        [Test]
        public void StringBuilderFinde()
        {


            Assert.AreEqual(new StringBuilder(" {{ping}}").IndexOf("{{", 0), 1);
            var str = new StringBuilder(@"12 123 1234 12345 123456 1234567");
            Assert.AreEqual(str.IndexOf("12", 0), 0);
            Assert.AreEqual(str.IndexOf("12", 9), 12);
            Assert.AreEqual(str.IndexOf("12", 12), 12);

        }

        [Test]
        public void NodesTemplate()
        {

            var list = TemplateParser.GetNodePacks("{{ a}}");
            list = TemplateParser.GetNodePacks("{{ a}} {{ b}}");
            list = TemplateParser.GetNodePacks("{{ #if true}} <o> {{ #end if}}");
            list = TemplateParser.GetNodePacks("{{a}} {{ #if true}}{{b}} 2P> {{ #end if}} test");
            list = TemplateParser.GetNodePacks("{{a}} {{ #if true}}{{b}} 2P> {{ #end if}} test {{a}} {{ #if true}}{{b}} 2P> {{ #end if}} test");
            list = TemplateParser.GetNodePacks("{{a}} {{ #if true}}{{b}} 2P> {{ #end if}} test {{a}} {{ #if true}}{{b}} {{#if bb}}PP{{#end if}} RNM {{ #end if}} test");
            list = TemplateParser.GetNodePacks("{{a}} {{ #if true}} {{#if p}} {{b}} {{#if p}} {{bb}} {{#if p}} {{b}} {{#end if}}  {{#enf if}}  {{#enf if}} {{ #end if}} test {{a}} {{ #if true}}{{b}} {{#if bb}}PP{{#end if}} RNM {{ #end if}} test");

            //todo say ok
        }

        [Test]
        public void CreateTagBuilder()
        {
            TagCore test = TemplateParser.CreateTagBuilder(@"{{ #if true}} <o> {{ #end if}}");
            test = TemplateParser.CreateTagBuilder("{{a}} {{ #if true}}{{b}} 2P> {{ #end if}} test {{a}} {{ #if true}}{{b}} {{#if bb}}PP{{#end if}} RNM {{ #end if}} test");
            test = TemplateParser.CreateTagBuilder("{{a}} {{a}} {{#if item}} if_text {{#else}} else_text {{#end if}}");
            test = TemplateParser.CreateTagBuilder("{{a}} {{ #if true}} test {{#each karr}} {{#if item}} if_text {{#else}} else_text {{#end if}} {{#end each}} {{ #end if}} test {{a}} {{ #if true}}{{b}} {{#if bb}}PP{{#end if}} RNM {{ #end if}} test");
        }

        [Test]
        public void KeyParesrs()
        {
            string b = string.Empty;
            string a = string.Empty;
            a = TemplateParser.GetSourceAndKey("{{#a}}", out b);
            Assert.AreEqual(a, "a");
            Assert.AreEqual(b, string.Empty);
            a = TemplateParser.GetSourceAndKey("{{#a   }}", out b);
            Assert.AreEqual(a, "a");
            Assert.AreEqual(b, string.Empty);
            a = TemplateParser.GetSourceAndKey("{{  #a  b.b.B }}", out b);
            Assert.AreEqual(a, "a");
            Assert.AreEqual(b, "b.b.B");
            a = TemplateParser.GetSourceAndKey("{{  #a  b.b.[2] }}", out b);
            Assert.AreEqual(a, "a");
            Assert.AreEqual(b, "b.b.[2]");
            a = TemplateParser.GetSourceAndKey("{{  #a  b.b.[2]$Now() }}", out b);
            Assert.AreEqual(a, "a");
            Assert.AreEqual(b, "b.b.[2]$Now()");
            a = TemplateParser.GetSourceAndKey("{{  #a  b.b.[2]<$Now(0,231,Dd.bb)*21 }}", out b);
            Assert.AreEqual(a, "a");
            Assert.AreEqual(b, "b.b.[2]<$Now(0,231,Dd.bb)*21");

        }

        [Test]
        public void TestIf()
        {
            var obj = @"{'a':'david','b':'daEEid','c':'anzor'}";
            TagCore tag = TemplateParser.CreateTagBuilder("{{#if a<>b}} {{a}} {{#end if}}");

            var str = tag.Build(obj).ToString();
            Assert.AreEqual(" david ", str);
            str = tag.BuildObject(new { a = "david", b = "abcd", c = "razor" }).ToString();
            Assert.AreEqual(" david ", str);
            obj = @"{'a':300}";
            tag = TemplateParser.CreateTagBuilder("{{#if a=300}} თვიური ლიმიტი აჭარბებს {{#else}} დღიური ლიმიტი {{#end if}}");
            str = tag.Build(obj).ToString();
            obj = @"{'a':'david','b':'daEEid','c':'anzor'}";
            tag = TemplateParser.CreateTagBuilder("{{#if $state(a)<>$state(b)}} {{a}} {{#end if}}");

            str = tag.Build(obj).ToString();
            Assert.AreEqual(" david ", str);

            obj = @"{'k':5,'c':{'l':{'m':9}},'b':{'t':{k:5},'g':{m:9}}}";
            tag = TemplateParser.CreateTagBuilder("{{#if b.t.k=5}}if ok{{#end if}}");
            str = tag.Build(obj).ToString();
            Assert.AreEqual("if ok", str);

            tag = TemplateParser.CreateTagBuilder("{{#if c.l.m=5}}if ok {{#else}}else ok{{#end if}}");
            str = tag.Build(obj).ToString();
            Assert.AreEqual("else ok", str);
            tag = TemplateParser.CreateTagBuilder("{{#if k=5}}if ok {{#else}}else ok{{#end if}}");

            str = tag.Build(obj).ToString();
            Assert.AreEqual("if ok ", str);

        }

        [Test]
        public void TagFaunder()
        {
            JObject obj = JObject.Parse(@"{'k':5,'c':{'l':{'m':9}},'b':{'t':{k:5},'g':{m:9}}}");
            Assert.AreEqual("15", (string)"b.g.m+1+k".ExecuteGetValue(obj.GetValue("c")));
            Assert.AreEqual("5", (string)"k".ExecuteGetValue(obj.GetValue("c")));
            Assert.AreEqual("9", (string)"l.m".ExecuteGetValue(obj.GetValue("c")));
            Assert.AreEqual("9", (string)"c.l.m".ExecuteGetValue(obj.GetValue("c")));
            Assert.AreEqual("9", (string)"b.g.m".ExecuteGetValue(obj.GetValue("c")));

        }

        [Test]
        public void SimpleTagBuild()
        {
            Tag tag = new Tag();
            string jobject = @"
{
fname:'გიორგი',
lname:'ქარდავა',
info:{phone:'+99500000000',email:'test@test.test'}
}";

            tag.InitializeSource(string.Empty, new StringBuilder(@" სალამი {{fname}} {{lname}}, მობილური {{info.phone}}"));
            Trace.Write(tag.Build(jobject).ToString());
        }

        [Test]
        public void TestEachWithNull()
        {
            TagCore tag = TemplateParser.CreateTagBuilder("{{#each A}}{{$state(A)}} {{#end each}}{{#each B}} $state(B) {{#end each}}");
            var result = tag.Build("{A:[1,2,3,4],B:null}");
            Assert.AreEqual("1 2 3 4 ", result.ToString());
        }


        [Test]
        public void EachSimpleItem()
        {

            TagCore tag = TemplateParser.CreateTagBuilder("{{#each key}} item[{{$index(key)}}]={{$state(key)}}{{#end each}}");
            var result = tag.Build("{key:[1,2,3,4]}");
            Assert.AreEqual(" item[0]=1 item[1]=2 item[2]=3 item[3]=4", result.ToString());
            tag = TemplateParser.CreateTagBuilder("{{#each key}} {{#each value}} {{$state(key)*$state(value)}} {{#end each}}{{#end each}}");
            result = tag.Build("{key:[1,2,3,4],value:[10,20,30,40]}");
            Assert.AreEqual("  10  20  30  40   20  40  60  80   30  60  90  120   40  80  120  160 ", result.ToString());

        }
        [Test]
        public void stateTest()
        {
            var tag = TemplateParser.CreateTagBuilder("{{#each k:key}}{{#if $index(k)%4=0 AND $index(k)>0 }}E{{#end if}}  {{$index(k)}}{{#end each}}");
            var result = tag.Build("{key:[1,2,3,4,5,6,7,8,9,10]}");
        }
        [Test]
        public void EachObjectItem()
        {

            TagCore tag = TemplateParser.CreateTagBuilder("{{#each key}} {{name}} {{fn_am_e}}{{#end each}}");
            var result = tag.Build("{key:[{name:'name1',fn_am_e:'fname1'},{name:'name2',fn_am_e:'fname2'}]}");
            Assert.AreEqual(" name1 fname1 name2 fname2", result.ToString());
        }

        [Test]
        public void EachTestBreak()
        {
            var tag = TemplateParser.CreateTagBuilder("{{#each b:key}} {{#each value}} {{#if $index(b)>2}} A {{  $index(b)}} {{#else}} B {{  $index(b)}}   {{#break}} {{#end if}} {{#end each}}{{#end each}}");
            var result = tag.Build("{key:[1,2,3,4],value:[10,20,30,40]}");
            Assert.AreEqual("   B 0      B 1      B 2      A 3    A 3    A 3    A 3  ", result.ToString());

        }

        [Test]
        public void EachTestContinue()
        {
            long max = 0, min = int.MaxValue;
            int MaxIter = 30000;
            for (int i = 0; i < 100; i++)
            {
                TagCore tag = TemplateParser.CreateTagBuilder("{{#each key}} {{#each value}} {{#if $index(key)>2}} A {{  $index(key)}} {{#else}} B {{  $index(key)}}   {{#continue}} {{#end if}} C {{$index(key)}} {{#end each}}{{#end each}}");
                var tick = Stopwatch.StartNew();
                var result = tag.Build("{key:[1,2,3,4],value:[10,20,30,40]}");
            }
            Thread.Sleep(5000);
            long[] arr = new long[MaxIter];
            for (int i = 0; i < MaxIter; i++)
            {
                var tick = Stopwatch.StartNew();
                var tag = TemplateParser.CreateTagBuilder("{{#each key}} {{#each value}} {{#if $index(key)>2}} A {{  $index(key)}} {{#else}} B {{  $index(key)}}   {{#continue}} {{#end if}} C {{$index(key)}} {{#end each}}{{#end each}}");
                var result = tag.Build("{key:[1,2,3,4],value:[10,20,30,40]}");
                tick.Stop();
                long time = tick.ElapsedMilliseconds;
                if (max < time)
                {
                    max = time;
                }
                if (min > time)
                {
                    min = time;
                }
                arr[i] = time;
                Assert.AreEqual("   B 0     B 0     B 0     B 0      B 1     B 1     B 1     B 1      B 2     B 2     B 2     B 2      A 3  C 3   A 3  C 3   A 3  C 3   A 3  C 3 ", result.ToString());
            }
            Assert.Fail("max: " + max + "     min:" + min + "\r\n");

        }


        [Test]
        public void ReEraserTest()
        {
            long max = 0, min = int.MaxValue;
            int MaxIter = 30000;
            for (int i = 0; i < 100; i++)
            {
                var tag = TemplateParser.CreateTagBuilder(@"{{#each $state()}}{{$state()}}!!{{#end each}}");
                var result = tag.Build("[1,2,3,4]");
            }
            Thread.Sleep(1000);
            var global = Stopwatch.StartNew();
            long[] arr = new long[MaxIter];
            for (int i = 0; i < MaxIter; i++)
            {
                var tick = Stopwatch.StartNew();
                var tag = TemplateParser.CreateTagBuilder(@"{{#each $state()}}{{$state()}}!!{{#end each}}");
                var result = tag.Build("[1,2,3,4]");
                tick.Stop();
                long time = tick.ElapsedMilliseconds;
                if (max < time)
                {
                    max = time;
                }
                if (min > time)
                {
                    min = time;
                }
                arr[i] = time;
            }
            global.Stop();
            Assert.Fail("max: " + max + "     min:" + min + "\r\n Global " + global.ElapsedMilliseconds);

        }



        [Test]
        public void MustacheStart()
        {
            long max = 0, min = int.MaxValue;
            int MaxIter = 30000;
            int[] arrTest = new int[4];
            for (int i = 0; i < 100; i++)
            {
                FormatCompiler compiler = new FormatCompiler();
                Generator generator = compiler.Compile(@"{{#each this}}{{this}}!!{{/each}}");
                string result = generator.Render(JArray.Parse("[1, 2, 3, 4]"));
                Console.Out.WriteLine(result);  // Hello, Bob!!!
            }
            Thread.Sleep(1000);
            long[] arr = new long[MaxIter];
            var global = Stopwatch.StartNew();
            for (int i = 0; i < MaxIter; i++)
            {
                var tick = Stopwatch.StartNew();
                FormatCompiler compiler = new FormatCompiler();
                Generator generator = compiler.Compile(@"{{#each this}}{{this}}!!{{/each}}");
                string result = generator.Render(arrTest);
                tick.Stop();
                long time = tick.ElapsedMilliseconds;
                if (max < time)
                {
                    max = time;
                }
                if (min > time)
                {
                    min = time;
                }
                arr[i] = time;
            }
            global.Stop();
            Assert.Fail("max: " + max + "     min:" + min + "\r\n Global " + global.ElapsedMilliseconds);

        }


        [Test]
        public void FnState()
        {
            TagCore tag = TemplateParser.CreateTagBuilder("{{$state()}}");
            var result = tag.Build("[1,2,3]");
            Assert.AreEqual(@"[
  1,
  2,
  3
]", result.ToString());
        }

        [Test]
        public void FunctionState()
        {
            TagCore tag = TemplateParser.CreateTagBuilder("{{#each $state()}} {{$state()}} {{#end each}}");
            var result = tag.Build("[1,2,3,4]");
            Assert.AreEqual(" 1  2  3  4 ", result.ToString());
            tag = TemplateParser.CreateTagBuilder("{{#each $state()}} {{#each $state()}}{{ $state() }} {{#end each}}{{#end each}}");
            result = tag.Build("[[1,1],[2,2],[3,3],[4,4]]");
            Assert.AreEqual(" 1 1  2 2  3 3  4 4 ", result.ToString());

        }

        [Test]
        public void EachWithDeclareValue()
        {
            var tag = TemplateParser.CreateTagBuilder("{{#each AB :$state()}} {{$state(AB)}} {{#end each}}");
            var result = tag.Build("[1,2,3,4]");
            Assert.AreEqual(" 1  2  3  4 ", result.ToString());
            tag = TemplateParser.CreateTagBuilder("{{#each AB:$state()}} {{#each Ac :$state()}}{{ $state(Ac) }} {{#end each}}{{#end each}}");
            result = tag.Build("[[1,1],[2,2]]");
            Assert.AreEqual(" 1 1  2 2 ", result.ToString());

        }


        [Test]
        public void CurrencyExecute()
        {
            var tag = TemplateParser.CreateTagBuilder("{{ $remove(1-week.usd/live.usd,5)}}");
            var result = tag.Build("{balance:{usd:13,btc:123},live:{usd:2.7,btc:9900},week:{usd:2.4,btc:8900}}");

            Assert.AreEqual("", result.ToString());
            tag = TemplateParser.CreateTagBuilder("{{#each AB:$state()}} {{#each Ac :$state()}}{{ $state(Ac) }} {{#end each}}{{#end each}}");
            result = tag.Build("[[1,1],[2,2]]");
            Assert.AreEqual(" 1 1  2 2 ", result.ToString());

        }


        [Test]
        public void FunctionLengthTest()
        {

            var result = HelpFuncs.ExecuteGetValue("$length(key)",
                JObject.Parse("{key:'iuuo',value:[10,20,30,40,33]}"),
                HelpFuncs.DynamicFunctions.Length);
            Assert.AreEqual("4", result);
            result = HelpFuncs.ExecuteGetValue("$length(key)",
                JObject.Parse("{key:[10,20,30,40,33],value:'iuuo'}"),
                HelpFuncs.DynamicFunctions.Length);
            Assert.AreEqual("5", result);
        }

        [Test]
        public void SimilarFunctionTest()
        {
            var result = HelpFuncs.ExecuteGetValue("$length(key1)>$length(key2)",
               JObject.Parse("{key1:'abcde',key2:'abcd'}"),
               HelpFuncs.DynamicFunctions.Length);
            Assert.AreEqual("True", result);
        }

        [Test]
        public void AsArrayFunctionTest()
        {
            var result = HelpFuncs.ExecuteGetValue("$asArray(key2)",
                  JObject.Parse("{key1:'abcde',key2:\"[1,2,3,4,5,6]\"}"),
                  HelpFuncs.DynamicFunctions.Length, HelpFuncs.DynamicFunctions.AsArray, HelpFuncs.DynamicFunctions.State);
            Assert.AreEqual("[\r\n  1,\r\n  2,\r\n  3,\r\n  4,\r\n  5,\r\n  6\r\n]", result);

            result = HelpFuncs.ExecuteGetValue("$length($asArray(key2))>$length(key2)",
               JObject.Parse("{key1:'abcde',key2:'[1,2,3,4,5,6]'}"),
               HelpFuncs.DynamicFunctions.Length);
            Assert.AreEqual("False", result);
        }


        [Test]
        public void ExecuteGetValue()
        {
            var result = HelpFuncs.ExecuteGetValue("key1",
                JObject.Parse("{key1:'06'}"),
                HelpFuncs.DynamicFunctions.Length, HelpFuncs.DynamicFunctions.AsArray, HelpFuncs.DynamicFunctions.State)
                .ToString();
            Assert.AreEqual("06", result);
        }

        [Test]

        public void TestMethod()
        {
            JToken tok = new JValue(DateTime.Now);
        }

        [Test]

        public void HelperFunction()
        {
            //TODO HELPER FUNCTION
            var result = HelpFuncs.StringCompile(" 100 < 94 ");

            Assert.AreEqual("False", result);
        }

        [Test]
        public void TestFile()
        {
            var file = File.ReadAllText("D:/A/t.tmail");
            var json = File.ReadAllText("D:/A/t.Json");
            TagCore tag = TemplateParser.CreateTagBuilder(file);
            File.WriteAllText("D:/A/t.html", tag.Build(json).ToString());
        }



    }
}
