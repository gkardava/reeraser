using Newtonsoft.Json.Linq;
using ReEraser.Parser;
using ReEraser.Tags;
using ReEraser.Tags.Render;
using ReEraser;            
using System.Text;
using System.IO;
using System;
using System.Windows.Forms;
using TesterForm;
public class TestBuild {

 [STAThread]
    static void Main(params string [] args) {
	if(args ==null || args.Length ==0)
	{
   Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
	
}          else
	{    if(args.Length !=4){
		Console.WriteLine("param1: template path,param2 jsonFilePath,Param3 result path");
		Console.ReadKey();
		    return;
	    }
       	StringBuilder template = new StringBuilder(File.ReadAllText(args[0]));
       	string json = File.ReadAllText(args[1]);
	long startTick=	DateTime.Now.Ticks;   
	
	try{
		TagCore tag = TemplateParser.CreateTagBuilder(template);
		for(int i=0;i<int.Parse(args[3]);i++)
		File.WriteAllText(String.Format(args[2],DateTime.Now.Ticks),tag.Build(json).ToString());
	}catch(Exception ex){
	Console.WriteLine(ex.Message);
	}
	Console.WriteLine("build time {0}",(DateTime.Now.Ticks-startTick)/(double)TimeSpan.TicksPerSecond);
	Console.ReadKey();
	}
    }
}
