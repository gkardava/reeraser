﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesterForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                OpenFileDialog dlg = new OpenFileDialog();
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    rtbTemplate.Text = dlg.FileName;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                SaveFileDialog dlg = new SaveFileDialog();
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    rtbResult.Text = dlg.FileName;
                }
            }
        }

        private void btnBuild_Click(object sender, EventArgs e)
        {
            StringBuilder bld = new StringBuilder();
            
            if (checkBox2.Checked)
            {
                bld.Append(File.ReadAllText(rtbTemplate.Text));
            }
            else
            {
                bld.Append(rtbTemplate.Text);
            }
            try
            {
                var tagBuildr = EtRender.Parser.TemplateParser.CreateTagBuilder(bld);
                bld = tagBuildr.Build(rtbJson.Text);
            }
            catch (Exception ex)
            {
                bld.Clear();
                bld.Append(ex.Message);

            }
            if (checkBox1.Checked)
            {
                File.WriteAllText(rtbResult.Text, bld.ToString());
            }
            else
            {
                rtbResult.Text = bld.ToString();
            }
            
        }
    }
}
